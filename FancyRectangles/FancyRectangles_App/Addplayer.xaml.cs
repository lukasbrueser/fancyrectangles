﻿using FancyRectangles_Model;
using FancyRectangles_ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FancyRectangles_App
{
    /// <summary>
    /// Interaktionslogik für Addplayer.xaml
    /// </summary>
    public partial class Addplayer : Window
    {
        public Addplayer()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SpielerVM sp = new SpielerVM(txtname.Text);
            SpielerVM.Spielerlist.Add(sp);
        }
    }
}
