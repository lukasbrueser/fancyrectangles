﻿using FancyRectangles_Model;
using FancyRectangles_ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FancyRectangles_App
{
    /// <summary>
    /// Interaktionslogik für Spieleroverview.xaml
    /// </summary>
    public partial class Spieleroverview : Window
    {
        SpielerVM currentsp;
        public Spieleroverview()
        {
            InitializeComponent();
        }

        private void btn_add(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            try
            {
                SpielerVM sp = new SpielerVM(txt.Text);
                for (int i = 0; i < 11; i++)
                {
                    sp.AddScore(r.Next(0, 100));
                }
                currentsp = sp;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            lstplayer.DataContext = SpielerVM.Spielerlist;
            lstplayer.SelectedIndex = lstplayer.Items.Count-1;
        }

        private void lstplayer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currentsp = lstplayer.SelectedItem as SpielerVM;
            DataContext = currentsp;
        }

        private void bttophs_Click(object sender, RoutedEventArgs e)
        {
            topav1.Visibility = Visibility.Hidden;
            topav2.Visibility = Visibility.Hidden;
            topav3.Visibility = Visibility.Hidden;
            tophs1.Visibility = Visibility.Visible;
            tophs2.Visibility = Visibility.Visible;
            tophs3.Visibility = Visibility.Visible;
        }
        private void bttopav_Click(object sender, RoutedEventArgs e)
        {
            topav1.Visibility = Visibility.Visible;
            topav2.Visibility = Visibility.Visible;
            topav3.Visibility = Visibility.Visible;
            tophs1.Visibility = Visibility.Hidden;
            tophs2.Visibility = Visibility.Hidden;
            tophs3.Visibility = Visibility.Hidden;
        }
    }
}
