﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FancyRectangles_App
{
    public class ConvHeight : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double reval=0;
            try
            {
                double score = double.Parse(value[0].ToString());
                double curheight = double.Parse(value[1].ToString());
                double hs = double.Parse(value[2].ToString());
                reval= (score / hs) * (curheight-80);
            }
            catch{ }
            return reval;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
