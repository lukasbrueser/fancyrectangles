﻿using FancyRectangles_Model;
using FancyRectangles_ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace FancyRectangles_App
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timerDirChange;
        DispatcherTimer timerRecGen;
        DispatcherTimer timerScore;
        DispatcherTimer timerCountdown;
        bool gamerunning = false;
        bool countdownrunning = false;
        bool sound = false;
        int score = 0;
        SpielerVM currentPlayer;
        readonly SoundPlayer sfx_pressbutton = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\long_low.wav"));
        readonly SoundPlayer sfx_pressbutton2 = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\long_high.wav"));
        readonly SoundPlayer sfx_error = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\error.wav"));
        readonly SoundPlayer sfx_ListBoxEnter = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\very_short_click.wav"));
        readonly SoundPlayer sfx_ListItem_Click = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\short_click.wav"));
        readonly SoundPlayer sfx_countdown_start = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\countdown.wav"));
        readonly SoundPlayer sfx_countdown_end = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\countdown2.wav"));
        readonly SoundPlayer sfx_death = new SoundPlayer(System.IO.Path.GetFullPath(@"Sounds\death.wav"));

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Spieler.Spielerlist.Open();
                listBoxSpieler.DataContext = SpielerVM.Spielerlist;
            }
            catch
            {

            }

            sound = true;
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            InitializeGame();
        }

        private void InitializeGame()
        {
            if (listBoxSpieler.SelectedItem == null)
            {
                lblError.Content = "Please choose a player.";
                sfx_error.Play();
                return;
            }

            if (sound)
                sfx_pressbutton2.Play();

            FadeColor(Colors.White, Colors.Black, can, 1000);
            string selecteddifficulty = (listBoxDifficulty.SelectedItem as ListBoxItem).Content.ToString();

            int spawninterval = 0;
            int dirchangeinterval = 0;
            int minscreentime = 0;
            int maxscreentime = 0;
            int minrecwidth = 0;
            int maxrecwidth = 0;
            int minrecheight = 0;
            int maxrecheight = 0;
            int scoreinterval = 0;

            switch (selecteddifficulty)
            {
                case "Easy":
                    spawninterval = 80;

                    dirchangeinterval = 15000;

                    minscreentime = 2000;
                    maxscreentime = 3000;

                    minrecwidth = 40;
                    maxrecwidth = 100;
                    minrecheight = 40;
                    maxrecheight = 100;

                    scoreinterval = 5000;
                    break;

                case "Normal":
                    spawninterval = 40;

                    dirchangeinterval = 10000;

                    minscreentime = 1500;
                    maxscreentime = 2500;

                    minrecwidth = 40;
                    maxrecwidth = 100;
                    minrecheight = 40;
                    maxrecheight = 100;

                    scoreinterval = 1000;
                    break;

                case "Difficult":
                    spawninterval = 20;

                    dirchangeinterval = 7000;

                    minscreentime = 1000;
                    maxscreentime = 2000;

                    minrecwidth = 40;
                    maxrecwidth = 100;
                    minrecheight = 40;
                    maxrecheight = 100;

                    scoreinterval = 700;
                    break;

                case "Impossible":
                    spawninterval = 20;

                    dirchangeinterval = 5000;

                    minscreentime = 700;
                    maxscreentime = 1700;

                    minrecwidth = 40;
                    maxrecwidth = 100;
                    minrecheight = 40;
                    maxrecheight = 100;

                    scoreinterval = 500;
                    break;

                case "Shuffle":
                    spawninterval = 60;

                    dirchangeinterval = 100;

                    minscreentime = 2000;
                    maxscreentime = 4000;

                    minrecwidth = 30;
                    maxrecwidth = 110;
                    minrecheight = 30;
                    maxrecheight = 110;

                    scoreinterval = 1000;
                    break;

                case "Small":
                    spawninterval = 15;

                    dirchangeinterval = 10000;

                    minscreentime = 1500;
                    maxscreentime = 2500;

                    minrecwidth = 10;
                    maxrecwidth = 50;
                    minrecheight = 10;
                    maxrecheight = 50;

                    scoreinterval = 1000;
                    break;
            }

            //Countdown
            ShowGameScreen();
            lblCountdown.Visibility = Visibility.Visible;
            lblCountdown.Content = "3";
            int count = 3;
            countdownrunning = true;
            timerCountdown = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(0.5)
            };

            timerCountdown.Tick += (s, ea) =>
            {
                if (sound)
                    sfx_countdown_start.Play();
                count--;
                lblCountdown.Content = count.ToString();
                if (count == 0)
                {
                    timerCountdown.Stop();
                    if (sound)
                        sfx_countdown_end.Play();
                    lblCountdown.Visibility = Visibility.Hidden;
                    FadeColor(Colors.Black, Colors.White, can, 1000);
                    StartGame(minrecwidth, maxrecwidth, minrecheight, maxrecheight, spawninterval, dirchangeinterval, minscreentime, maxscreentime, scoreinterval);
                    countdownrunning = false;
                }
            };

            timerCountdown.Start();
        }

        private void StartGame(int minrecwidth, int maxrecwidth, int minrecheight, int maxrecheight, int spawninterval,
                               int dirchangeinterval, int minscreentime, int maxscreentime, int scoreinterval)
        {
            ShowGameScreen();

            score = 0;
            lblScore.Content = $"Score: {score}";
            gamerunning = true;

            string direction = "down";
            timerDirChange = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(dirchangeinterval)
            };
            timerDirChange.Tick += (s, e) =>
            {
                Random rnd = new Random();
                switch (rnd.Next(0, 4))
                {
                    case 0:
                        direction = "down";
                        timerRecGen.Interval = TimeSpan.FromMilliseconds(spawninterval);
                        break;
                    case 1:
                        direction = "up";
                        timerRecGen.Interval = TimeSpan.FromMilliseconds(spawninterval);
                        break;
                    case 2:
                        direction = "left";
                        timerRecGen.Interval = TimeSpan.FromMilliseconds(spawninterval * can.ActualWidth / can.ActualHeight);
                        break;
                    case 3:
                        direction = "right";
                        timerRecGen.Interval = TimeSpan.FromMilliseconds(spawninterval * can.ActualWidth / can.ActualHeight);
                        break;
                }
            };
            timerDirChange.Start();

            timerRecGen = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(spawninterval)
            };
            timerRecGen.Tick += (s, ea) =>
            {
                Rechteck myRec = new Rechteck(minrecwidth, maxrecwidth, minrecheight, maxrecheight);
                myRec.AddToCanvas(can, direction, minscreentime, maxscreentime);
                myRec.Rectangle.MouseEnter += (d, eb) =>
                {
                    Death();
                };
            };
            timerRecGen.Start();

            timerScore = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(scoreinterval)
            };
            timerScore.Start();
            timerScore.Tick += (s, e) =>
            {
                score++;
                lblScore.Content = $"Score: {score}";
            };
        }

        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {
            if (gamerunning)
                Death();
            else if (countdownrunning)
            {
                countdownrunning = false;
                lblCountdown.Content = null;
                timerCountdown.Stop();
                FadeColor(Colors.Black, Colors.White, can, 1000);
                ShowMainMenu();
            }
        }

        private void Death()
        {
            currentPlayer.AddScore(score);

            if (sound)
                sfx_death.Play();
            gamerunning = false;
            ShowStatisticScreen();

            timerRecGen.Stop();
            timerDirChange.Stop();
            timerScore.Stop();

            Rectangle[] recs = (from c in can.Children.OfType<Rectangle>()
                                select c).ToArray();
            for (int i = 0; i < recs.Length; i++)
            {
                can.Children.Remove(recs[i]);
            }
        }

        private void FadeColor(Color c1, Color c2, Canvas canvas, int animationTime)
        {
            ColorAnimation ani = new ColorAnimation
            {
                Duration = TimeSpan.FromMilliseconds(animationTime),
                From = c1,
                To = c2
            };
            canvas.Background = new SolidColorBrush(c1);
            canvas.Background.BeginAnimation(SolidColorBrush.ColorProperty, ani);
        }

        private void ShowMainMenu()
        {
            gridSpieler.Visibility = Visibility.Hidden;
            gridGame.Visibility = Visibility.Hidden;
            gridStatistik.Visibility = Visibility.Hidden;
            gridMenu.Visibility = Visibility.Visible;
            lblError.Content = null;
        }

        private void ShowStatisticScreen()
        {
            gridStatistik.DataContext = currentPlayer;
            gridStatistik.Visibility = Visibility.Visible;
            gridSpieler.Visibility = Visibility.Hidden;
            gridGame.Visibility = Visibility.Hidden;
            gridMenu.Visibility = Visibility.Hidden;
        }

        private void ShowGameScreen()
        {
            lblScore.Content = null;
            gridSpieler.Visibility = Visibility.Hidden;
            gridStatistik.Visibility = Visibility.Hidden;
            gridGame.Visibility = Visibility.Visible;
            gridMenu.Visibility = Visibility.Hidden;
        }

        private void ShowPlayerScreen()
        {
            lblPlayerError.Content = null;

            gridSpieler.Visibility = Visibility.Visible;
            gridStatistik.Visibility = Visibility.Hidden;
            gridGame.Visibility = Visibility.Hidden;
            gridMenu.Visibility = Visibility.Hidden;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton2.Play();
            textBoxSpieler.Text = null;
            ShowMainMenu();
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            listBoxSpieler.DataContext = null;
            try
            {
                _ = new SpielerVM(textBoxSpieler.Text);
            }
            catch (Exception exp)
            {
                if (sound)
                    sfx_error.Play();

                listBoxSpieler.DataContext = SpielerVM.Spielerlist;
                lblPlayerError.Content = exp.Message;

                return;
            }
            if (sound)
                sfx_pressbutton2.Play();

            textBoxSpieler.Text = null;
            listBoxSpieler.DataContext = SpielerVM.Spielerlist;
            listBoxSpieler.SelectedIndex = listBoxSpieler.Items.Count - 1;
            ShowMainMenu();
        }

        private void BtnPlayer_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton.Play();
            ShowPlayerScreen();
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ListBoxItem_MouseEnter(object sender, MouseEventArgs e)
        {
            if (sound)
                sfx_ListBoxEnter.Play();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sound)
                sfx_ListItem_Click.Play();

            if (listBoxSpieler == null || listBoxSpieler.SelectedItem == null)
                return;
            Spieler zw = listBoxSpieler.SelectedItem as Spieler;
            currentPlayer = (SpielerVM)zw;
        }

        private void BtnSound_Click(object sender, RoutedEventArgs e)
        {
            sound = !sound;
            if (sound)
                BtnSound.Content = "Sound: ON";
            else
                BtnSound.Content = "Sound: OFF";

            if (sound)
                sfx_pressbutton2.Play();
        }

        private void BtnRetry_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton.Play();

            InitializeGame();
        }

        private void BtnMenu_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton2.Play();

            ShowMainMenu();
        }

        private void Btntophs_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton2.Play();

            topav1.Visibility = Visibility.Hidden;
            topav2.Visibility = Visibility.Hidden;
            topav3.Visibility = Visibility.Hidden;
            tophs1.Visibility = Visibility.Visible;
            tophs2.Visibility = Visibility.Visible;
            tophs3.Visibility = Visibility.Visible;
        }
        private void Btntopav_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton2.Play();

            topav1.Visibility = Visibility.Visible;
            topav2.Visibility = Visibility.Visible;
            topav3.Visibility = Visibility.Visible;
            tophs1.Visibility = Visibility.Hidden;
            tophs2.Visibility = Visibility.Hidden;
            tophs3.Visibility = Visibility.Hidden;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Spieler.Spielerlist.Save();
        }

        private void BtnViewStatistic_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton2.Play();

            if (currentPlayer == null)
            {
                lblError.Content = "Please choose a player to view the statistics";
                return;
            }
            ShowStatisticScreen();
        }

        private void BtnDeletePlayer_Click(object sender, RoutedEventArgs e)
        {
            if (sound)
                sfx_pressbutton2.Play();

            if (currentPlayer == null)
            {
                lblError.Content = "Please choose a player delete someone";
                return;
            }

            SpielerVM.Spielerlist.RemoveAt(listBoxSpieler.SelectedIndex);
            currentPlayer = null;
        }
    }
}
