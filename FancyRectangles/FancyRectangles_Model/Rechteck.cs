﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace FancyRectangles_Model
{
	public class Rechteck
	{
		private static readonly Random rnd = new Random();
		private readonly int minwidth;
		private readonly int maxwidth;
		private readonly int minheight;
		private readonly int maxheight;
		readonly Rectangle myRectangle = new Rectangle();

		public double Width
		{
			get
			{
				return rnd.Next(minwidth, maxwidth);
			}
		}

		public double Height
		{
			get
			{
				return rnd.Next(minheight, maxheight);
			}
		}

		public Rectangle Rectangle
		{
			get
			{
				return myRectangle;
			}
		}

		public Rechteck(int minwidth, int maxwidth, int minheight, int maxheight)
		{
			if (minwidth > maxwidth)
				throw new Exception("minimum width cannot be higher than maximum width");

			if (minheight > maxheight)
				throw new Exception("minimum height cannot be higher than maximum height");

			this.minwidth = minwidth;
			this.maxwidth = maxwidth;
			this.minheight = minheight;
			this.maxheight = maxheight;
		}

		public void AddToCanvas(Canvas can, string direction, int mintime, int maxtime)
		{
			if (mintime > maxtime)
				throw new Exception("minimum animation time cannot be higher than maximum time");

			myRectangle.Width = Width;
			myRectangle.Height = Height;

			Panel.SetZIndex(myRectangle, 1);

			SolidColorBrush randomColor;
			do
			{
				randomColor = new SolidColorBrush(
					Color.FromRgb((byte)rnd.Next(130, 255), (byte)rnd.Next(130, 255), (byte)rnd.Next(130, 255)));
			}
			while (randomColor.Color.R > 200 && randomColor.Color.G > 200 && randomColor.Color.B > 200); //weiß bzw. ganz helle rechtecke vermeiden

			myRectangle.Fill = randomColor;

			can.Children.Add(myRectangle);

			DoubleAnimation myAni = new DoubleAnimation();

			myAni.Completed += (s, e) =>
			{
				can.Children.Remove(myRectangle);
			};

			switch (direction)
			{
				case "down":
					Canvas.SetTop(myRectangle, -myRectangle.Height);
					Canvas.SetLeft(myRectangle, rnd.Next(-(int)myRectangle.Width, (int)can.ActualWidth));

					myAni.From = Canvas.GetTop(myRectangle);
					myAni.To = can.ActualHeight;

					myAni.Duration = new Duration(TimeSpan.FromMilliseconds(rnd.Next(mintime, maxtime)));

					myRectangle.BeginAnimation(Canvas.TopProperty, myAni);
					break;
				case "up":
					Canvas.SetBottom(myRectangle, -myRectangle.Height);
					Canvas.SetLeft(myRectangle, rnd.Next(-(int)myRectangle.Width, (int)can.ActualWidth));

					myAni.From = can.ActualHeight;
					myAni.To = Canvas.GetBottom(myRectangle);

					myAni.Duration = new Duration(TimeSpan.FromMilliseconds(rnd.Next(mintime, maxtime)));

					myRectangle.BeginAnimation(Canvas.TopProperty, myAni);
					break;
				case "right":
					Canvas.SetTop(myRectangle, rnd.Next(-(int)myRectangle.Height, (int)can.ActualHeight));
					Canvas.SetLeft(myRectangle, -myRectangle.Width);

					myAni.From = Canvas.GetLeft(myRectangle);
					myAni.To = can.ActualWidth;

					myAni.Duration = new Duration(TimeSpan.FromMilliseconds(rnd.Next(mintime, maxtime) * can.ActualWidth / can.ActualHeight));

					myRectangle.BeginAnimation(Canvas.LeftProperty, myAni);
					break;
				case "left":
					Canvas.SetTop(myRectangle, rnd.Next(-(int)myRectangle.Height, (int)can.ActualHeight));
					Canvas.SetRight(myRectangle, -myRectangle.Width);

					myAni.From = can.ActualWidth;
					myAni.To = Canvas.GetRight(myRectangle);

					myAni.Duration = new Duration(TimeSpan.FromMilliseconds(rnd.Next(mintime, maxtime) * can.ActualWidth / can.ActualHeight));

					myRectangle.BeginAnimation(Canvas.LeftProperty, myAni);
					break;
				default:
					throw new Exception($"\"{direction}\" is not a valid direction");
			}
		}
	}
}
