﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FancyRectangles_Model
{
    public class Spieler
    {
        private string name;
        private int highscore;
        private readonly List<int> scores = new List<int>();
        private static readonly Spielerliste spielerlist = new Spielerliste();

        public int Highscore
        {
            get { return highscore; }
            set
            {
                try
                {
                    highscore = value;
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }
        public double Average
        {
            get
            {
                try
                {
                    return scores.Average();
                }
                catch
                {
                    return 0;
                }
            }
        }
        public static Spielerliste Spielerlist
        {
            get { return spielerlist; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public List<int> Scores
        {
            get { return scores; }
        }
        public void AddScore(int score)
        {
            if (score>highscore)
            {
                highscore = score;          //Neuen Highscore setzen.
            }
            if (scores.Count>9)
            {
                scores.RemoveAt(9);         //Letzten Score entfernen
                scores.Insert(0, score);    //neuesten Score hinzufügen
            }
            else
            {
                scores.Insert(0, score);
            }
        }
        public Spieler(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Please enter a valid player name!");
            }
            bool identical = false;
            for (int i = 0; i < spielerlist.Count; i++)
            {
                if (name == spielerlist[i].name)
                    identical = true;
            }
            if (identical)
            {
                throw new Exception("A player with this name already exists!");
            }
            else
            {
                this.name = name;
            }
        }
        public override string ToString()
        {
            return name;
        }
        public string Serialize()
        {
            string serialize = "";
            serialize += $"{name}";
            serialize += $";{highscore}";
            for (int i = 0; i < scores.Count; i++)
            {
                serialize += $";{scores[i]}";       //Die letzten 10 Spiele werden abgespeichert. Falls keine 10 Daten vorhanden sind, weniger, deswegen als letzter Datensatz (macht die Übersicht einfacher)
            }
            return serialize;
        }
        public static Spieler Parse(string data)
        {
            string[] tokens = data.Split(';');
            Spieler sp = new Spieler(tokens[0]);
            sp.Highscore = int.Parse(tokens[1]);
            for (int i = 2; i < tokens.Length; i++)
            {
                sp.AddScore(int.Parse(tokens[i]));
            }
            return sp;
        }
    }
}
