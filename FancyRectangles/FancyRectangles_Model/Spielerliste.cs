﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FancyRectangles_Model
{
    public class Spielerliste : ObservableCollection<Spieler>
    {
        public void Save()
        {
            StreamWriter sw = new StreamWriter("general.csv");
            foreach (Spieler item in this)
            {
                sw.WriteLine(item.Serialize());
            }
            sw.Dispose();
        }

        public void Open()
        {
            Clear();
            StreamReader sr = new StreamReader("general.csv");
            while (!sr.EndOfStream)
            {
                Spieler.Spielerlist.Add(Spieler.Parse(sr.ReadLine()));
            }
        }
    }
}
