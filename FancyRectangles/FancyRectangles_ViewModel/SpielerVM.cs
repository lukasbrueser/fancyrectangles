﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FancyRectangles_ViewModel
{
    public class SpielerVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private FancyRectangles_Model.Spieler sp;
        private static SpielerlisteVM spielerlist = new SpielerlisteVM();

        public int Highscore
        {
            get
            {
                return sp.Highscore;
            }
        }
        public FancyRectangles_Model.Spieler Model
        {
            get { return sp; }
        }
        public double Average
        {
            get
            {
                return sp.Average;
            }
        }

        public List<int> Scores
        {
            get
            {
                return sp.Scores;
            }
        }
        public string Name
        {
            get { return sp.Name; }
        }
        public static SpielerlisteVM Spielerlist
        {
            get { return spielerlist; }
        }
        public ObservableCollection<FancyRectangles_Model.Spieler> Topav    //Rückgabe der Top Spieler (Average)
        {
            get
            {
                ObservableCollection<FancyRectangles_Model.Spieler> tops = new ObservableCollection<FancyRectangles_Model.Spieler>(spielerlist.Items.OrderByDescending(x => x.Average));
                return tops;
            }
        }
        public ObservableCollection<FancyRectangles_Model.Spieler> Tophs    //Rückgabe der Top Spieler (Highscore)
        {
            get
            {
                ObservableCollection<FancyRectangles_Model.Spieler> tops = new ObservableCollection<FancyRectangles_Model.Spieler>(spielerlist.Items.OrderByDescending(x => x.Highscore));
                return tops;
            }
        }



        public SpielerVM(string name)
        {
            sp = new FancyRectangles_Model.Spieler(name);
            spielerlist.Add(this);
            OnPropertyChanged("Tophs");
            OnPropertyChanged("Topav");
            OnPropertyChanged("Spielerlist");
        }
        public SpielerVM() { }
        public void AddScore(int value)
        {
            sp.AddScore(value);
            OnPropertyChanged("Average");
            OnPropertyChanged("Highscore");
            OnPropertyChanged("Sp");
            OnPropertyChanged("tophs");
            OnPropertyChanged("topav");
            OnPropertyChanged("Name");
            OnPropertyChanged("Scores");
        }
        public override string ToString()
        {
            return Name;
        }
        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        public static implicit operator SpielerVM(FancyRectangles_Model.Spieler spieler)
        {
            _ = new SpielerVM();
            return new SpielerVM()
            {
                sp = spieler
            };
        }
    }
}
