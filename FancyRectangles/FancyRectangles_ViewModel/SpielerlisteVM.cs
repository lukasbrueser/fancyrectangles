﻿using FancyRectangles_Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FancyRectangles_ViewModel
{
    public class SpielerlisteVM
    {
        public FancyRectangles_Model.Spielerliste Items { get; private set; } = FancyRectangles_Model.Spieler.Spielerlist;

        public void Add(SpielerVM item)
        {
            Items.Add(item.Model);
        }

        public void RemoveAt(int index)
        {
            Items.RemoveAt(index);
        }
    }
}
